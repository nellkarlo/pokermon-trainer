import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';


import { AppComponent } from './app.component';
import { LibraryComponent } from './components/library/library.component';
import { LandingComponent } from './components/landing/landing.component';
import { TrainerComponent } from './components/trainer/trainer.component';
import { LibraryItemComponent } from './components/library-item/library-item.component';
import { TrainerItemComponent } from './components/trainer-item/trainer-item.component';

import { AppRoutingModule } from './app-routing.module';

@NgModule({
  declarations: [
    AppComponent,
    LandingComponent,
    LibraryComponent,
    TrainerComponent,
    LibraryItemComponent,
    TrainerItemComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
