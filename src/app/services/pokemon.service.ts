import { Injectable } from "@angular/core";
import { HttpClient, HttpErrorResponse } from '@angular/common/http'

import { Pokemon } from "../models/pokemon.model";
import { PokemonResult } from "../models/pokemon-result.model";
import { BehaviorSubject, Observable } from "rxjs";
import { User } from "../models/user.model";
import { LocalStorageService } from "./localstorage.service";
import { Router } from "@angular/router";
@Injectable({
    providedIn: 'root'
})

export class PokemonService {
    private _pokemons$: BehaviorSubject<Pokemon[]> = new BehaviorSubject<Pokemon[]>([]);
    private _fetchedPokemons: Pokemon[] = []
    
    // variable to keep track if pokemons has been fetched. 
    private _hasDownloadedPokemons: boolean = false;
    private _error: string = ''

    constructor(private readonly http: HttpClient,
         private readonly localStorageService: LocalStorageService,
         private readonly router: Router) { }


    // Fetches the 1st gen pokemons from PokeAPI. Checks if Pokemons are already downloaded, if they are skips it
    // then maps the fetched pokemons to the users caught ones and sets isCaught to true on the ones user has caught.
    // Assigns fetchedpokemons to pokemons BehaviorSubject to be able to track changes 
    public fetchPokemons(): void {
        //fetches 1st gen (151)
        this.http.get<PokemonResult>('https://pokeapi.co/api/v2/pokemon/?offset=0&limit=151')
            .subscribe((response: PokemonResult) => {
                
                if (!this._hasDownloadedPokemons) {
                    response.results.forEach((pokemon) => {
                        this.fetchPokemon(pokemon.url)
                    })
                }
                this._hasDownloadedPokemons = true
                

                this._fetchedPokemons.map((pokemon)=>{
                    this.localStorageService.getUser().pokemons.map((storedPokemon)=>{
                        if(storedPokemon.id === pokemon.id){
                            pokemon.isCaught = true;
                            return pokemon;
                        } else return
                        
                    }
                    )
                })

            }, (error: HttpErrorResponse) => {
                this._error = error.message;
                this.localStorageService.clearUser();
                alert(`Something went wrong. Redirecting you to homepage! Error: ${error.message}`)
                this.router.navigateByUrl('/')
            })

            this._pokemons$.next(this._fetchedPokemons);
    }

    get getPokemons$(): Observable<Pokemon[]> {
        return this._pokemons$.asObservable();
    }
    public getError(): string {
        return this._error;
    }

    public fetchPokemon(url: string): void {
        this.http.get<Pokemon>(url)
            .subscribe((pokemon: Pokemon) => {
                pokemon.isCaught = false
                this._fetchedPokemons.push(pokemon)
            }, (error: HttpErrorResponse) => {
                this._error = error.message;
            })

    }
    

    // Sets the pokemon to caught then calls savePokemons method
    public setHasPokemon(id: number) {
        this._pokemons$.next(this._pokemons$.getValue().map((pokemon: Pokemon) => {
            if (pokemon.id === id) {
                pokemon.isCaught = !pokemon.isCaught
            }
            return pokemon
        }))
        this.savePokemons()

    }

    // Saves the current state of caught pokemons to the users localstorage
    public savePokemons(){
        let user: User = this.localStorageService.getUser()
        user.pokemons = this._pokemons$.getValue().filter(pokemon=>{
            if(pokemon.isCaught) return pokemon;
            else return
        })
        this.localStorageService.setUser(user)
    }

    // resets the pokemons-array, pokemons observable and sets hasdownloaded to false
    public setHasDownloadedReset(){
        this._fetchedPokemons = []
        this._pokemons$.next([])
        this._hasDownloadedPokemons = false;
    }

}



