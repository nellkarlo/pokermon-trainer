import { Injectable } from "@angular/core";
import { User } from "../models/user.model";

@Injectable({
    providedIn: 'root'
})

export class LocalStorageService {

    // LocalStorage key
    private _LS_KEY = 'PokeApp'

    // Gets the user if it's not null and returns it
    // else returns empty user
    public getUser(): User {
        if (localStorage.getItem(this._LS_KEY) !== null) {
            const localUser = JSON.parse(localStorage.getItem(this._LS_KEY)!)

            let user: User = {
                name: localUser.name,
                pokemons: localUser.pokemons
            }
            return user
        }
        //return empty user if no user found in localstorage
        let user: User = {
            name: '',
            pokemons: []
        }
        return user
    }
    // Sets the user to the provided user object.
    public setUser(user: User): void {
        localStorage.setItem(this._LS_KEY, JSON.stringify(user))
       
    }
    public clearUser():void{
        localStorage.removeItem(this._LS_KEY)
    }

}