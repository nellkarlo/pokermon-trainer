export interface Pokemon {
    id: number,
    name: string,
    abilities: []
    sprites: Sprites,
    url: string,
    isCaught?: boolean,
    stats: Stats[]      
}

export interface Sprites{
    front_default: string, 
}
export interface Stats{
    base_stat: number,
    stat: Stat
}
export interface Stat{
    name: string
}