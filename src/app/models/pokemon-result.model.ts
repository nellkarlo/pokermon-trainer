import { Pokemon } from "./pokemon.model";

export interface PokemonResult{
    count: number;
    next: string;
    previous: string;
    results: Pokemon[]
}