import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { LandingComponent } from "./components/landing/landing.component";
import { LibraryComponent } from "./components/library/library.component";
import { TrainerComponent } from "./components/trainer/trainer.component";

// routes
const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: '/landing'
  },
  {
    path: 'landing',
    component: LandingComponent
  }, {
    path: 'library',
    component: LibraryComponent
  },
  {
    path: 'trainer',
    component: TrainerComponent
  }

]
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }