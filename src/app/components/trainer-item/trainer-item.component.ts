import { Component, EventEmitter, Input, Output } from "@angular/core";
import { Pokemon } from "src/app/models/pokemon.model";
import { PokemonService } from "src/app/services/pokemon.service";

@Component({
    selector: 'app-trainer-item',
    templateUrl: './trainer-item.component.html',
    styleUrls: ['./trainer-item.component.css',]
})

export class TrainerItemComponent {

    constructor(private readonly pokemonService: PokemonService) { }
    
    @Input() pokemon: Pokemon = {
        id: 0,
        name: '',
        url: '',
        abilities: [],
        sprites: {
            front_default: ''
        },
        isCaught: false,
        stats: [{
            base_stat: 0,
            stat: { name: '' }
        }]
    }
    @Output() clicked: EventEmitter<Pokemon> = new EventEmitter();

    onPokemonClicked() {
        this.clicked.emit(this.pokemon);
    }
}