import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { Pokemon } from "src/app/models/pokemon.model";
import { LocalStorageService } from "src/app/services/localstorage.service";
import { PokemonService } from "src/app/services/pokemon.service";

@Component({
    selector: 'app-trainer',
    templateUrl: './trainer.component.html',
    styleUrls: ['./trainer.component.css',]
})

export class TrainerComponent implements OnInit {
    constructor(private readonly pokemonService: PokemonService,
        private readonly localStorageService: LocalStorageService,
        private readonly router: Router) { }
    public pokemons: Pokemon[] = []
    public _hasDownloaded: boolean = false

    ngOnInit(): void {

        // On reload failsafe to alert user changes will be lost
        window.addEventListener("beforeunload", function (e) {
            var confirmationMessage = "\o/";
            e.returnValue = confirmationMessage;     // Gecko, Trident, Chrome 34+
            return confirmationMessage;              // Gecko, WebKit, Chrome <34
        });

        // Navigation check
        if (this.localStorageService.getUser().name !== '') {
            this.router.navigateByUrl('/trainer');
        } else { this.router.navigateByUrl('/') }

        // check so that pokemons dont fetch again if they are already downloaded
        if (!this._hasDownloaded) {
            this.pokemonService.fetchPokemons();
            this._hasDownloaded = true;
        }

        // subscribe to observable (only the ones that isCaught)
        this.pokemonService.getPokemons$.subscribe((data) => {
            this.pokemons = data.filter((pokemon) => {
                if (pokemon.isCaught === true) return pokemon;
                else return;
            })
        })


    }
    // set the pokemon to !isCaught
    handlePokemonClicked(pokemon: Pokemon) {
        this.pokemonService.setHasPokemon(pokemon.id);
    }

    // On Logout, clears user, clears observable, fetchedpokemons and sets hasdownloaded to false in pokemonservice.
    // Then nagivages to startpage. 
    handleLogoutClicked() {
        this.localStorageService.clearUser();
        this.pokemonService.setHasDownloadedReset();
        this.router.navigateByUrl('/');
    }

}