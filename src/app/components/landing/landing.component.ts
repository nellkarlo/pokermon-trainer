import { Component, OnInit } from "@angular/core";
import { NgForm } from "@angular/forms";
import { User } from "src/app/models/user.model";
import { Router } from '@angular/router'
import { LocalStorageService } from "src/app/services/localstorage.service";
import { Pokemon } from "src/app/models/pokemon.model";


@Component({
    selector: 'app-landing',
    templateUrl: './landing.component.html',
    styleUrls: ['./landing.component.css',]
})

export class LandingComponent implements OnInit {
    constructor(private readonly router: Router,
        private localStorageService: LocalStorageService) { }

    // Log in method, checks if user is logged; goes to library. Else goes back to startpage
    public onSubmit(user: string): void {

        let User: User = {
            name: user,
            pokemons: []
        }
        if (!user) {
            this.router.navigateByUrl(`/`)
            return;
        }
        this.localStorageService.setUser(User)
        this.router.navigateByUrl(`/library`)
    }

    ngOnInit() {
        if (this.localStorageService.getUser().name !== '') {
            this.router.navigateByUrl('/library')
        } else { this.router.navigateByUrl('/') }

    }

    get user(): User | undefined {
        return this.localStorageService.getUser()
    }
}