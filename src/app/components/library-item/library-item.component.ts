import { Component, Input, Output, EventEmitter } from "@angular/core";
import { Router } from "@angular/router";
import { Pokemon } from "src/app/models/pokemon.model";
import { PokemonService } from "src/app/services/pokemon.service";

@Component({
    selector: 'app-library-item',
    templateUrl: './library-item.component.html',
    styleUrls: ['./library-item.component.css',]
})

export class LibraryItemComponent {
    constructor(private readonly pokemonService: PokemonService,
        private readonly router: Router,
    ) { }

    get routerUrl() {
        return this.router.url
    }
    @Input() pokemon: Pokemon = {
        id: 0,
        name: '',
        url: '',
        abilities: [],
        sprites: {
            front_default: '',
        },
        isCaught: false,
        stats: [
            {
                base_stat: 0,
                stat: {
                    name: '',
                }
            }
        ]
    }
    @Output() clicked: EventEmitter<Pokemon> = new EventEmitter();

    // Emits the pokemon that is clicked to parent component
    public onPokemonClicked(): void {
        this.clicked.emit(this.pokemon);
    }
}