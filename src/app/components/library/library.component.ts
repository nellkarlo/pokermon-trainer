import { Component, NgZone, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { Pokemon } from "src/app/models/pokemon.model";
import { User } from "src/app/models/user.model";
import { LocalStorageService } from "src/app/services/localstorage.service";

import { PokemonService } from "src/app/services/pokemon.service";


@Component({
   selector: 'app-catalogue',
   templateUrl: './library.component.html',
   styleUrls: ['./library.component.css',]
})

export class LibraryComponent implements OnInit {
   constructor(private readonly pokemonService: PokemonService,
      private readonly router: Router,
      private readonly localStorageService: LocalStorageService
   ) { }
   public pokemons: Pokemon[] = [];
   public _hasDownloaded: boolean = false

   ngOnInit(): void {

      if (this.localStorageService.getUser().name === '') {
         this.router.navigateByUrl('/');
      }
      // On reload failsafe to alert user changes will be lost
      window.addEventListener("beforeunload", function (e) {
         var confirmationMessage = "\o/";
         e.returnValue = confirmationMessage;     // Gecko, Trident, Chrome 34+
         return confirmationMessage;              // Gecko, WebKit, Chrome <34
     });


      if (!this._hasDownloaded) {
         this.pokemonService.fetchPokemons();
         this._hasDownloaded = true;
      }

      


      this.pokemonService.getPokemons$.subscribe((data) => {
         this.pokemons = data;
      })


   }

// sets isCaught to true when pokemons is clicked on
   handlePokemonClicked(pokemon: Pokemon): void {
      this.pokemonService.setHasPokemon(pokemon.id);
   }
}